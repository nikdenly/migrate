<?php

class OtzyvyMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
   
    //DB
    Database::addConnectionInfo('for_migration', 'default', array(
      'driver' => 'mysql',
      'database' => 'ndenly_photo',
      'username' => 'ndenly_photo',
      'password' => 'root',
      'host' => 'localhost',
      'prefix' => '',
    ));  
    
    //Sourse
    $query = Database::getConnection('default', 'for_migration')
        ->select('mskru_documents', 'p');
    $query->innerJoin('mskru_document_fields', 'join1', 'p.id = join1.document_id AND join1.rubric_field_id = :rubric_field_1', array(':rubric_field_1' => 270));
    $query->innerJoin('mskru_document_fields', 'join2', 'p.id = join2.document_id AND join2.rubric_field_id = :rubric_field_2', array(':rubric_field_2' => 271));
    $query->innerJoin('mskru_document_fields', 'join3', 'p.id = join3.document_id AND join3.rubric_field_id = :rubric_field_3', array(':rubric_field_3' => 266));
    $query->innerJoin('mskru_document_fields', 'join4', 'p.id = join4.document_id AND join4.rubric_field_id = :rubric_field_4', array(':rubric_field_4' => 268));
    $query->innerJoin('mskru_document_fields', 'join5', 'p.id = join5.document_id AND join5.rubric_field_id = :rubric_field_5', array(':rubric_field_5' => 269));
    $query->fields('p', array('id', 
                              'document_title', 
                              'document_alias', 
                              'document_breadcrum_title', 
                              'document_published',
                              'document_meta_keywords',
                              'document_meta_description',
                              ));
    $query->addField('join1', 'field_value', 'order_min');
    $query->addField('join2', 'field_value', 'body');
    $query->addField('join3', 'field_value', 'banner');
    $query->addField('join4', 'field_value', 'title');
    $query->addField('join5', 'field_value', 'title2');
    $query->condition('p.rubric_id', 38);
    //$query->range(0, 100);
    $this->source = new MigrateSourceSQL($query, array(), NULL, array(
      'map_joinable' => FALSE));
    
    //Destination
    $this->destination = new MigrateDestinationNode('basic_page');
    
    //Перезапись
    //$this->systemOfRecord = Migration::DESTINATION;
    
    //Key schema
    $source_key_schema = array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      )
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key_schema, MigrateDestinationTerm::getKeySchema());
  
    //Mapping
    $this->addFieldMapping('pathauto')->defaultValue('0');
    $this->addFieldMapping('language')->defaultValue('ru');
    $this->addFieldMapping('path', 'document_alias');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('created', 'document_published');
    $this->addFieldMapping('field_name2', 'title2');
    $this->addFieldMapping('metatag_title', 'document_title');
    $this->addFieldMapping('metatag_keywords', 'document_meta_keywords');
    $this->addFieldMapping('metatag_description', 'document_meta_description');
    $this->addFieldMapping('field_for_breadcrumb', 'document_breadcrum_title');
    $this->addFieldMapping('field_order_min', 'order_min');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')->defaultValue('full_html');
    $this->addFieldMapping('field_banner', 'banner'); //Основная картинка
    //$this->addFieldMapping('field_portfolio_mainimage:file_replace')->defaultValue(FILE_EXISTS_ERROR);
    $this->addFieldMapping('field_banner:source_dir')->defaultValue('https://www.mosreklama.net/');
    
  }
  
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
     
    // подключаем баннер
    $links = $row->banner;
    $pieces = explode('"', $links);
    $link = $pieces[1];  
    $row->banner = $link;
    
    //Если заголовка нет
    if (!$row->title) {
      if (!$row->title2) {
        $row->title = $row->document_title;
      }
      else { 
        $row->title = $row->title2;
      }
    }
    
    //если нет метатегов крошек
    if (!$row->document_title) {
      $row->document_title = $row->title;
    }
    
    //Если хлебные крошки пустые
    if (!$row->document_breadcrum_title) {
      $row->document_breadcrum_title = $row->title;
    }
  }
} 

