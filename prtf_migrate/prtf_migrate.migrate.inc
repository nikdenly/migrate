<?php

/**
 * Implement hook_migrate_api()
 */

function prtf_migrate_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'NodeMigrate' => array(
        'title' => 'Nodes',
      ),
      'TermMigrate' => array (
        'title' => 'Terms',
      ),
    ),
    'migrations' => array(
      'Prtf' => array(
        'class_name' => 'Prtf',
        'group_name' => 'NodeMigrate',
      ),
      'BasepageMigration' => array(
        'class_name' => 'BasepageMigration',
        'group_name' => 'NodeMigrate',
      ),
      'BasepagenewMigration' => array(
        'class_name' => 'BasepagenewMigration',
        'group_name' => 'NodeMigrate',
      ),
      'VacancyMigration' => array(
        'class_name' => 'VacancyMigration',
        'group_name' => 'NodeMigrate',
      ),
      /*'OtzyvyMigration' => array(
        'class_name' => 'OtzyvyMigration',
        'group_name' => 'NodeMigrate',
      ),*/
      'RegionMigration' => array(
        'class_name' => 'RegionMigration',
        'group_name' => 'NodeMigrate',
      ),
    ),
  );
  return $api;
} 
