<?php

class Prtf extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
   
    Database::addConnectionInfo('for_migration', 'default', array(
      'driver' => 'mysql',
      'database' => 'ndenly_photo',
      'username' => 'ndenly_photo',
      'password' => 'root',
      'host' => 'localhost',
      'prefix' => '',
    )); 
    
    // Source
    $query = Database::getConnection('default', 'for_migration')
        ->select('mskru_documents', 'p');
    $query->innerJoin('mskru_document_fields', 'join1', 'p.id = join1.document_id AND join1.rubric_field_id = :rubric_field_1', array(':rubric_field_1' => 224));
    $query->innerJoin('mskru_document_fields', 'join2', 'p.id = join2.document_id AND join2.rubric_field_id = :rubric_field_2', array(':rubric_field_2' => 232));
    $query->innerJoin('mskru_document_fields', 'join3', 'p.id = join3.document_id AND join3.rubric_field_id = :rubric_field_3', array(':rubric_field_3' => 60));
    $query->innerJoin('mskru_document_fields', 'join4', 'p.id = join4.document_id AND join4.rubric_field_id = :rubric_field_4', array(':rubric_field_4' => 223));
    $query->innerJoin('mskru_document_fields', 'join5', 'p.id = join5.document_id AND join5.rubric_field_id = :rubric_field_5', array(':rubric_field_5' => 236));
    $query->innerJoin('mskru_document_fields', 'join6', 'p.id = join6.document_id AND join6.rubric_field_id = :rubric_field_6', array(':rubric_field_6' => 237));
    $query->innerJoin('mskru_document_fields', 'join7', 'p.id = join7.document_id AND join7.rubric_field_id = :rubric_field_7', array(':rubric_field_7' => 235));
    $query->innerJoin('mskru_document_fields', 'join8', 'p.id = join8.document_id AND join8.rubric_field_id = :rubric_field_8', array(':rubric_field_8' => 61));
    $query->innerJoin('mskru_document_fields', 'join9', 'p.id = join9.document_id AND join9.rubric_field_id = :rubric_field_9', array(':rubric_field_9' => 62));
    $query->innerJoin('mskru_document_fields', 'join10', 'p.id = join10.document_id AND join10.rubric_field_id = :rubric_field_10', array(':rubric_field_10' => 63));
    $query->innerJoin('mskru_document_fields', 'join11', 'p.id = join11.document_id AND join11.rubric_field_id = :rubric_field_11', array(':rubric_field_11' => 252));
    $query->innerJoin('mskru_document_fields', 'join12', 'p.id = join12.document_id AND join12.rubric_field_id = :rubric_field_12', array(':rubric_field_12' => 256));
    $query->innerJoin('mskru_document_fields', 'join13', 'p.id = join13.document_id AND join13.rubric_field_id = :rubric_field_13', array(':rubric_field_13' => 255));
    $query->innerJoin('mskru_document_fields', 'join14', 'p.id = join14.document_id AND join14.rubric_field_id = :rubric_field_14', array(':rubric_field_14' => 254));
    $query->fields('p', array('id', 'document_title', 'document_alias', 'document_breadcrum_title', 'document_published'));
    $query->addField('p', 'document_published', 'year');
    $query->addField('join1', 'field_value', 'brand_name');
    $query->addField('join2', 'field_value', 'brand');
    $query->addField('join3', 'field_value', 'adv_type');
    $query->addField('join4', 'field_value', 'weight');
    $query->addField('join5', 'field_value', 'time');
    $query->addField('join6', 'field_value', 'place');
    $query->addField('join7', 'field_value', 'sector');
    $query->addField('join8', 'field_value', 'body');
    $query->addField('join9', 'field_value', 'attachment');
    $query->addField('join10', 'field_value', 'images_attachment');
    $query->addField('join11', 'field_value', 'is_in_portf');
    $query->addField('join12', 'field_value', 'audio');
    $query->addField('join13', 'field_value', 'video');
    $query->addField('join14', 'field_value', 'is_in_right');
    $query->condition('p.rubric_id', 16);
    $query->range(207, 10);
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
 
    // Destination
    
    $this->destination = new MigrateDestinationNode('portfolio');
 
    // Key schema
    $source_key_schema = array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      )
    );
    $this->map = new MigrateSQLMap($this->machineName, $source_key_schema, MigrateDestinationNode::getKeySchema());
 
    // Mapping
    $this->addFieldMapping('field_prtf_id', 'id');
    $this->addFieldMapping('pathauto')->defaultValue('0');
    $this->addFieldMapping('language')->defaultValue('ru');
    $this->addFieldMapping('path', 'document_alias');
    $this->addFieldMapping('title', 'document_title');
    $this->addFieldMapping('field_for_breadcrumb', 'document_breadcrum_title');
    $this->addFieldMapping('field_portfolio_brand_name', 'brand_name');
    $this->addFieldMapping('field_portfolio_brand', 'brand');
    $this->addFieldMapping('field_portfolio_types_main', 'adv_type');
    $this->addFieldMapping('field_prtf_weight', 'weight');
    $this->addFieldMapping('field_prtf_time', 'time');
    $this->addFieldMapping('field_portfolio_place', 'place'); 
    $this->addFieldMapping('created', 'document_published'); //дата создания
    $this->addFieldMapping('field_prtf_year', 'year'); // год
    $this->addFieldMapping('field_portfolio_sector', 'sector');
    $this->addFieldMapping('body', 'body');
    $this->addFieldMapping('body:format')->defaultValue('full_html');
    //$this->addFieldMapping('field_portfolio_mainimage', 'attachment'); //Основная картинка
    //$this->addFieldMapping('field_portfolio_mainimage:file_replace')->defaultValue(FILE_EXISTS_ERROR);
    //$this->addFieldMapping('field_portfolio_mainimage:source_dir')->defaultValue('https://www.mosreklama.net/');
    //$this->addFieldMapping('field_prft_image_more', 'images_attachment'); //доп картинки
    //$this->addFieldMapping('field_prft_image_more:file_replace')->defaultValue(FILE_EXISTS_ERROR);
    //$this->addFieldMapping('field_prft_image_more:source_dir')->defaultValue('https://www.mosreklama.net/');
    $this->addFieldMapping('field_is_in_prtf', 'is_in_portf')->defaultValue('0');
    //$this->addFieldMapping('field_prtf_audio', 'audio'); // аудио
    //$this->addFieldMapping('field_prtf_audio:source_dir')->defaultValue('https://www.mosreklama.net/');
    //$this->addFieldMapping('field_prtf_video', 'video'); // видео
    //$this->addFieldMapping('field_prtf_video:source_dir')->defaultValue('https://www.mosreklama.net/');
    $this->addFieldMapping('field_is_in_right', 'is_in_right');
    
    $this->timeThreshold = 0.5;
  }
  
  
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    
    // показывать справа
    include 'leftblock.php';
    if (strripos($row->is_in_right, ',') != FALSE) {
      $rightarr = explode(', ', $row->is_in_right); 
      $count = count($rightarr);
      for ($i=0; $i<$count; $i++) {
        foreach ($leftblock as $key => $massiv1) {
          if ($rightarr[$i] == $key) {
            $rightarr[$i] = $massiv1;
          }
        }
      }
      $rightarr = array_unique($rightarr);
      $row->is_in_right = array_values($rightarr);
    }
    else {
      foreach ($leftblock as $key => $massiv1) {
        if (mb_strtolower($key) == mb_strtolower($row->is_in_right)) {
          $row->is_in_right = $massiv1;
        }
      }
    }
    
    
    // Подключаем галочку в портфолио
    if ($row->is_in_portf == 'Да') {
      $row->is_in_portf = TRUE;
    }
    else {
      $row->is_in_portf = FALSE;
    }
    
    
    
    //Подключаем термины Бренды
    $termid = $row->brand;
      $brands = Database::getConnection('default', 'for_migration')->select ('mskru_documents', 'p')
        ->fields('p', array('document_title'))
        ->condition('p.id', $termid)
        ->execute();
    foreach ($brands as $brand) {
      $row->brand = $brand->document_title; 
    }
      
    //Подключаем термины типы Рекламы
    include 'reklama_types.php';
    if (strripos($row->adv_type, ',') != FALSE) {
      $arr = explode(', ', $row->adv_type); 
      $count = count($arr);
      for ($i=0; $i<$count; $i++) {
        foreach ($reklama as $key => $massiv1) {
          foreach ($massiv1 as $item => $massiv2) {
            foreach ($massiv2 as $list => $value) {
              if ($arr[$i] == $value) {
                $arr[$i] = $item;
              }
            }
          }
        }
      }
      $arr = array_unique($arr);
      $row->adv_type = array_values($arr);
    }
    else {
      foreach ($reklama as $key => $massiv1) {
        foreach ($massiv1 as $item => $massiv2) {
          foreach ($massiv2 as $list => $value) {
            if (mb_strtolower($value) == mb_strtolower($row->adv_type)) {
              $row->adv_type = $item;
            }
          }
        }
      }
    }
    
    //Подключаем год
    $row->year = date('Y', $row->year);
        
    //Подключаем отрасль
    include 'sectorarray.php';
    $sectorid = $row->sector;
    $sectoritem;
      $sectors = Database::getConnection('default', 'for_migration')->select ('mskru_documents', 'p')
        ->fields('p', array('document_title'))
        ->condition('p.id', $sectorid)
        ->execute();
    foreach ($sectors as $sector) {
      $sectoritem = $sector->document_title; 
    }
    foreach ($sectorsarray as $key => $item) {
      if ($sectoritem == $item) {
        $row->sector = $key;
      }
    }
        
    // Загружаем изображения
    $links = $row->images_attachment;
    $pieces = explode('"', $links);
    foreach ($pieces as $key => $item) {
      if ((strripos($item, ':') != FALSE) || (strripos($item, '}') != FALSE)) {
        unset($pieces[$key]);
      }
    }
    $link = array_values($pieces);  
    $row->images_attachment = $link;
    
    //Если поле Имя пустое
    if (!$row->document_title) {
      $row->document_title = $row->document_breadcrum_title;
    }
    
    //Если поле Хлебных крошек пустое
    if ($row->document_breadcrum_title) {
      $row->document_breadcrum_title = $breadcrumb;
    }
    else {
      $row->document_breadcrum_title = $row->document_title;
    }
  }
}

